```DRAFTv6, MARTIN OWENS, LUCA FRESCHI 2019-05-31```

<img align="left" src="https://gitlab.com/doctormo/evcf/raw/master/docs/evcf.svg">

This document defines how the VCF genetic format should be extended to include a concise set of useful meta data about the genetic records in the rest of the document. This enriched format is documented here to help create an agreed exchange of data in studies.

This extends version 4.2[1] of the VCF format with new and useful field formats for add in-place meta data about strains.

[1] https://gist.github.com/inutano/f0a2f5c219ab4920c5b5

Preface
=======

Meta data in the VCF format is stored in the header file and should follow these instructions (see vcf 4.2 for reference). Each new field format below this adds it's data to the header of a VCF format using the standard VCF header data format that should already be readable by most VCF reading modules.

It's not expected that researchers would write these files manually but use provided tools and modules to add and modify data mechanically. This description is thus given to help programmers make compatible readers and writers.

* Lines must appear after the `##fileformat` directive and before Record header line.
* Each header line starts with two hash symbols `##`
* Each header lines should contain a name, value pair separated by an equals sign.
* All names together should be collected into a dictionary/hash structure.
* Names should conform to the range of upper case A-Z and the under-line symbol
* Names may appear multiple times, in this case each value should be collected into a list.
* Simple Values are string values that continue until the end of the line.
* Complex Values may be dictionary/hash structures specified as:
  * Name value pair separated by an equals sign
  * Multiple pairs, each pair separated with commas
  * The whole list of value pairs between a less than and greater than sign.
  * Names and values must not contain any of the symbols used in definition.
  * No escaping feature is provided in the VCF format but strings in this format can be escaped using RFC-3986 and compatible readers should be able to decode these strings.

Data Fields
===========

Each of these fields are added to the VCF header using the rules above. Dictionary sub types are listed below in their own sections.

|Key|Count|Required|Data|Description|
|---|-----|--------|----|-----------|
|`ENRICHED`|One|*Yes*|String|Version of the standard being used. The format noted here should simply say '1.0'. The version and use of this format is noted in the ENRICHED header and should be used as the indicator to quickly tell if the file has been enriched with meta data in the given format. If it is missing, the file is considered just a normal vcf file.|
|`ISOLATE_NAME`|One|*Yes*|String|Unique name used to identify the isolate.|
|`LOCATION`|One|*Yes*|Dictionary|[see below](#location-data)|
|`CREATED`|One|No|Date|ISO date when this sample/isolate was created.|
|`NOTE`|Many|No|String|Any other information about the isolate/sample can be given in the notes section. These notes should be specific to the isolate as a whole and not to the study or resistance sub-data.|
|`LAB_NAME`|One|No|String|Name of the lab this isolate is from.|
|`RUN_ID`|Many|No|String|The unique IDs used for each of the RUNS this isolate is created from. If you downloaded the sequencing data from a public database to generate the VCF you want to enrich, you should specify the run_IDs you used here (e.g. SRR1165516,SRR1165581). |
|`SAMPLE_ID`|One|No|String|The public ID which identifies this data with a databank. For instance, if your isolate is present on NCBI, this field would correspond to the BIOSAMPLE. If missing, data should be considered non-public unless otherwise known.|
|`PROJECT_ID`|Many|No|String|The public ID of any known projects this sample is collected under. For instance, if your isolate is present on NCBI, this field would correspond to the BIOPROJECT.|
|`STRAIN_ID`|One|No|String|A unique ID used to identify other isolates which together will make a strain. This field is rarely used and most of the time ISOLATE_NAME is sufficient.|
|`GENEBANK`|One|No|Enumeration|The public location where the sample is known. Can be: NCBI,EBI,DDBJ,OTHER|
|`LINEAGE`|One|No|String|A list of categories separated by periods from most generic to most specific. For example lineage4.9.2 in TB would be "4.9.2" in this field and would represent main lineage of 4, and sub-lineage 4.9 etc. Where lineages have names, separate each name with a period.|
|`DRUG`|Many|No|Dictionary|[see below](#resistance-phenotype-data)|
|`PATIENT`|One|No|Dictionary|[see below](#patient-data)|
|`STUDY`|Many|No|Dictionary|[see below](#studypaper-data)|

Location Data
-------------

This required data should contain location information for the sample/isolate about where it was collected (not where it was sequenced or where it was analysed). It should NOT include data specific to a patient or otherwise identifiable information.

|Key|Required|Data|Description|
|---|--------|----|-----------|
|`COUNTRY`|*Yes*|String|Name of the country from the UN/LOCODE Countries List.|
|`STATE`|No|String|Province or state inside the country. It should attempt to use the UN/LOCODE name and spelling for consistency.|
|`CITY`|No|String|Name of the city inside the Country given above. It should attempt to use the UN/LOCODE city name and spelling for consistency.|
|`LOCODE`|No|String|The LOCODE designation for this country (two letters) or country and city (five letters) see UN/LOCODE list.|
|`LON`|No|Decimal|Longitude location of the City if available.|
|`LAT`|No|Decimal|Latitude location of the City if available.|

Resistance Phenotype Data
-------------------------

Resistance phenotype information is used to describe real world tests conducted against this isolate to decide if the isolate is resistant to a drug or not.

|Key|Required|Data|Description|
|---|--------|----|-----------|
|`NAME`|*Yes*|String|Name of the drug or common abbreviation.|
|`STATUS`|*Yes*|Enumeration|Was this isolate found to be resistant. S=Sensitive, R=Resistant, U=Unknown|
|`MEDIA`|No|String|Specific growth media used. i.e. m7h10, LJ, etc|
|`CRITCON`|No|Decimal in µg/ml|The critical concentration considered for this test.|
|`TESTCON`|No|Decimal in µg/ml|The tested concentration for MIC experiments (CONC).|
|`METHOD`|No|Enumeration|The test method used. DST=Drug Susceptibility Testing, MIC=Minimal Inhibitory Concentration|
|`NOTE`|No|String|A single line about the test performed.|

Patient Data
------------

|Key|Required|Data|Description|
|---|--------|----|-----------|
|`ID`|No|String|An unidentifiable patient ID used in data collation.|
|`SEX`|No|Enumeration|Single letter sex of the patient. M=Male,F=Female,U=Unknown,O=Other|
|`AGE`|No|Integer range|Age or Age range given as an integer or two integers separated with a dash symbol.|
|`HIV`|No|Enumeration|Patients HIV status as a single letter. U=Unknown,P=Positive,N=Negative.|
|`NOTE`|No|String|Single line about the patient.|

Study/Paper Data
----------------

Store any and all papers or non-paper study this data is used in, if there's existing papers, keep them and add another entry for your own paper reference as you use the data.

|Key|Required|Data|Description|
|---|--------|----|-----------|
|`NAME`|*Yes*|String|None unique name of the paper or study.|
|`URL`|*Yes*|URL String|Internet address where the paper or study is hosted.|
|`DOI`|No|The Paper's DOI, not used for study only.|

Converting to Tabulated Formats
===============================

Many tools, scripts and processes require that this information be presented to them without the mutation/vcf data or require that it be presented in a tabulated way. For this we suggest this set of headers cross referenced into a number of tables where needed. All formats are the same as the enriched fields above.

All blank fields (where data does not exist) may be denoted with an empty string and should be considered as `NULL`.

Text fields with multiple lines may encode their line feeds with RFC-3986 notation (%A0) or by using enclosed double quotes (see RFC-4180) readers should be able to decode both.

Details Tabulated File
----------------------

A csv or tsv file containing the following headers considered case sensitive. This is a combination of the [Data Fields](#data-fields), [Location Data](#location-data), [Patient Data](#patient-data) and [Study/Paper Data](#studypaper-data) tables above.

`ISOLATE_ID|SAMPLE_ID|RUN_IDs|PROJECT_IDs|STRAIN_ID|CREATED|COUNTRY|STATE|CITY|LOCODE|LON|LAT|NOTEs|LAB_NAME|GENEBANK|LINEAGE|PATIENT_ID|SEX|AGE|HIV|PATIENT_NOTE|STUDY_NAME|STUDY_URL|STUDY_DOI`

|Key|Description|
|---|-----------|
|RUN_IDs|A space separated list of RUN_ID strings.|
|PROJECT_IDs|A space separated list of PROJECT_ID strings.|
|NOTEs|A concatenation of all NOTE entries.|

Phenotype Tabulated File
------------------------

A csv or tsv file containing the following headers considered case sensitive. All fields come from the [Resistance Phenotype Data](#resistance-phenotype-data) table above.

`ISOLATE_ID|DRUG_NAME|STATUS|MEDIA|CRITCON|TESTCON|METHOD|NOTE`

Converting to Structured Formats
================================

It may be useful to separate the VCF data from the meta data presented here. To do so you may use `yaml` or `json` to represent the dictionary structure or a list of dictionary structures of the above fields exactly as denoted above. `XML` may be used using a json to xml converter, but is not covered by this document.

Fields which are flat scalars (strings, integers, etc) should be represented as such in the data. While fields that contain sub-fields such as `LOCATION` should be represented with a dictionary. Fields which allow for **Many** of that field should use a list structure for example `PROJECT_ID` in the example below.

Structured Example
------------------

```[
  {
    'ISOLATE_NAME': 'EIN440001',
    'LOCATION': {
        'COUNTRY': 'UK',
        ...
    }
    'PROJECT_ID': ['PRJ001', 'PRJ005', ...],
    'NOTE': ['NOTE ONE', 'NOTE TWO', ...],
    ...
  }
]```
