#!/usr/bin/env python3
#
# Copyright (C) 2019 Martin Owens
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#
# pylint: disable=bad-whitespace
#
"""Setup for enrich module"""

from setuptools import setup
from enrich import __version__, __pkgname__

with open('README.md') as fhl:
    DESC = fhl.read()

setup(
    name             = __pkgname__,
    version          = __version__,
    description      = 'Data management tools',
    long_description = DESC,
    author           = 'Martin Owens',
    url              = 'https://gitlab.com/doctormo/python_enrich',
    author_email     = 'doctormo@gmail.com',
    scripts          = [
        'bin/weld',
        'bin/distill',
    ],
    test_suite       = 'tests',
    platforms        = 'linux',
    license          = 'AGPLv3',
    include_package_data = True,
    package_dir = {
        'enrich': 'enrich',
    },
    install_requires = [
        'translitcodec==0.4.0',
        'tabulate==0.8.2',
        'xssd>=0.9.2',
    ],
    classifiers      = [
        'Development Status :: 0 - Test Only',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
)
