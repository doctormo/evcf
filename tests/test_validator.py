#
# Copyright (C) 2018 Martin Owens
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#
"""
Test that the validator is valid (and works)
"""

import os
import unittest

from enrich.validator import MetaDataValidator, ValidationStats

TEST_ROOT = os.path.abspath(os.path.dirname(__file__))
SCHEMA = os.path.join(TEST_ROOT, 'data', 'schema.xsd')

class ValidatorTestCase(unittest.TestCase):
    """Test validating vcf structures"""
    maxDiff = 9380

    def setUp(self):
        """Create machines needed to test"""
        self.val = MetaDataValidator(SCHEMA)

    def validate(self, dat):
        return self.val.validate({'root': {'strain': dat}})['strain']

    def explain_validation(self, dat):
        return self.val.explain_validation({'root': {'strain': dat}})

    def test_01_blank_structure(self):
        """Test the errors from a blank structure"""
        self.assertIn('strain_name', self.validate({}))
        self.assertIn('Invalid Required (Data is required, but missing.)', self.explain_validation({}))

    def test_02_minimum(self):
        """Test the smallest valid evcf datum"""
        self.assertFalse(self.validate({'strain_name': 'test02', 'location': {'country': 'GB'}}))

    def test_03_maximum(self):
        """Test no errors from a fully filled out structure"""
        self.assertFalse(self.validate({
            'enriched': '1.0',
            'created': '2018-11-01',
            'strain_name': 'test03',
            'lab_name': 'LSTM',
            'run_id': ['run01', 'run02', 'run02'],
            'sample_id': 'sample01',
            'project_id': ['project01', 'project02'],
            'genebank': 'NCBI',
            'lineage': '3.4.2a',
            'drug': [
                {'name': 'ETHAMBUTOL', 'status': 'S', 'media': 'Chalk', 'critcon': 10,
                 'method': 'MIC', 'note': 'Everything died'},
                {'name': 'AMIKACIN', 'status': 'R', 'media': 'Cheese', 'critcon': 50,
                 'method': 'DST', 'note': 'Everyone likes cheese'},
                {'name': 'CAPREOMYCIN', 'status': 'U', 'media': 'Bread', 'critcon': 4.5,
                 'method': 'DST', 'note': 'Lab techs ate the bread before testing could start.'},
            ],
            'patient': {
                'id': 'patient01',
                'sex': 'U',
                'age': 55,
                'hiv': 'N',
                'note': 'Patient tried to eat the bread with the cheese, but was too slow.',
            },
            'study': {
                'name': 'Chalk or Cheese in TB testing labs?',
                'url': 'https://not.a.real.place.ac.uk/test03.html',
                'doi': 'Department of substance 2018, et al.',
            },
            'location': {
                'country': 'US',
                'state': 'Massachusetts',
                'city': 'Boston',
                'locode': '',
                'lon': 34.5677, 'lat': 45.3332,
            },
            'note': [
                'This strain was a midnight strain to Georga.',
                'It was full of coal so we burried it.',
            ],
        }))

    def test_20_location(self):
        """Test location information"""
        def assertLocation(location):
            self.assertFalse(self.val.validate({'strain_name': 'test03', 'location': location}))
        def assertNotLocation(location):
            self.assertTrue(self.val.validate({'strain_name': '!test03', 'location': location}))

        #assertLocation({'country': 'GB'})
        #assertLocation({'country': 'United States of America'})
        #assertLocation({'country': 'US'})
        #assertLocation({'country': 'USA'})

        #assertLocation({'country': 'GB', 'state': 'Cheshire'})
        #assertLocation({'country': 'GB', 'state': ''})

    def test_07_multi_stats(self):
        """Test the various stats of multiple runs"""
        ret1 = self.validate(
            {'strain_name': 'test01', 'location': {'country': 'GB'}}
        )
        ret2 = self.validate(
            {'strain_name': 'test02',}
        )
        ret3 = self.validate(
            {'strain_name': 'test03', 'location': {'country': 'GB'}, 'created': '2018-11-01'}
        )
        ret4 = self.validate(
            {'strain_name': 'test04', 'location': {'country': 'GB'}, 'created': '2018-11-AA'}
        )
        ret5 = self.validate(
            {'strain_name': 'test05', 'location': {'country': 'GB'}, 'patient': {'sex': 'J', 'note': ['Note A', 'Note B']}}
        )
        stats = ValidationStats([ret1, ret2, ret3, ret4, ret1, ret1, ret4, ret5])
        self.assertEqual('\n' + stats.stats(), """
  created (invalid_pattern: 2)
  location (invalid_required: 1)
    note (invalid_occurs: 1)
    sex (invalid_enum == None: 1)
""")
