#
# Copyright (C) 2019 Dr. Maha Farhat
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#
"""
Test the mutations API
"""

import os
import unittest

from enrich.mutate import get_mutator, get_mutation_api, apply_mutation, generate_mutator

TEST_ROOT = os.path.abspath(os.path.dirname(__file__))

class FiltersTestCase(unittest.TestCase):
    """Test basic flat functionality of lookups."""
    def test_get_api(self):
        """API dictionary contains the right items"""
        api = get_mutation_api()
        self.assertIn('tr', api)
        self.assertNotIn('__doc__', api)

    def test_apply_func_mutation(self):
        """Applying mutations"""
        def _mut(item):
            item.append('doc')
            return 'marty'

        ret = []
        self.assertEqual(apply_mutation(_mut, item=ret), 'marty')
        self.assertEqual(len(ret), 1)
        self.assertEqual(ret[0], 'doc')

    def test_apply_script_mutation(self):
        """Run a script test"""
        filename = os.path.join(TEST_ROOT, 'data', 'mut.py')
        ret = ['foo', 1, 2, 3]
        apply_mutation(filename, item=ret, direct_api=False)
        self.assertEqual(len(ret), 1)
        self.assertEqual(ret[0], 'bar')
        ret = ['goo', 1, 2, 3]
        apply_mutation(filename, item=ret, direct_api=False)
        self.assertEqual(len(ret), 1)
        self.assertEqual(ret[0], 'goo')

    def test_get_mut(self):
        """Can turn a filename into a function"""
        filename = os.path.join(TEST_ROOT, 'data', 'mut.py')
        caller = get_mutator(filename)
        self.assertTrue(callable(caller))

    def test_direct_api(self):
        """Test basic direct api caller"""
        script = get_mutator('anon0', """
output = {
    'a': item[0].item_one,
    'b': item[0].item_2.forty,
    'c': index,
    'd': n0,
}
""")
        self.assertRaises(AttributeError, script)
        ret = script(item={
            'My Little Title': [
                {
                    'item óne': 4,
                    'item 2': {'forty': 42},
                },
            ],
            'index': 9,
            0: "odd index",
        })
        self.assertEqual(ret, {'a': 4, 'b': 42, 'c': 9, 'd': 'odd index'})

    def test_generate_mutator(self):
        """Generate a new mutator script"""
        ret = generate_mutator([
            {
                'ID': 4,
                'A': {'foo': 6, 'bar': 9.2},
                'B': "Sumptuous",
                'C': [5, 6, 7],
            },{
                'ID': 5,
                'A': {'goo': [1, 2, 3]},
                'D': "Hail and Wind",
            }
        ])
        lines = [line for line in ret.split("\n") if not line.strip().startswith('#')]
        self.assertEqual('\n'.join(lines), """
output = {
"ID": 4,
"A": {
  "foo": 6,
  "bar": 9.2,
},
"B": 'Sumptuous',
"C": [...],
}
""")
