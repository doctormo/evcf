#
# Copyright (C) 2019 Dr. Maha Farhat
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#
"""
Test the Weld functionality.
"""

import os
import unittest

from enrich.io import ListOfItems

TEST_ROOT = os.path.abspath(os.path.dirname(__file__))

class ParsimonyBasicTests(unittest.TestCase):
    """Basic type detections"""
    def test_csv_reading(self):
        """Test that csv file data can be retyped"""
        loi = ListOfItems.from_name('tests/data/weld_a.csv')
        pack = loi.by_unique_key('ID')
        self.assertEqual(len(pack), 8)
        self.assertEqual(type(pack[1]['ID']), int)
        self.assertEqual(type(pack[1]['Primary Language']), str)
        self.assertEqual(type(pack[1]['Developing']), bool)
        self.assertEqual(type(pack[1]['Rating']), float)
        # How to deal with null/empty values
        self.assertEqual(pack[7]['Country'], None)
        self.assertEqual(pack[7]['Developing'], None)
        self.assertEqual(pack[7]['Rating'], None)
        # But empty strings are still possible.
        self.assertEqual(pack[8]['Country'], '')


class ParsimonyAdvancedTests(object): #unittest.TestCase):
    """Data can be further classified with automatic schemas"""
    def setUp(self):
        self.data = ListOfItems.from_name('tests/data/countries.csv')

#
# This is the information about EACH column that's important:
# type: basic-type (see above)
# unique: True or False
# notnull: True or False
# choices: None or list of small set of choices.
#


#
# To test generation of multi-layered schemas we must weld a bunch of
# different data together.
#
# This seems like it might be different that the above data.
#

    def test_column_types(self):
        """Columns are given a schema type"""
        # Label each column's types (int, float, string, lists-of, etc)
        raise NotImplementedError("")

    def test_column_lists(self):
        """Column is given a schema list-of basic type"""
        raise NotImplementedError("")

    def test_column_dicts(self):
        """Column is given a schema children"""
        # columns are combined dictionary and sub-dictionaries
        raise NotImplementedError("")

    def test_column_list_of_dicts(self):
        """Column is given a schema list-of child dictionaries"""
        raise NotImplementedError("")

    def test_unique_keys(self):
        """Unique key column detected"""
        # Integer or non-space string columns only:
        # unique keys: No values are null, values are all different.
        raise NotImplementedError("")

    def test_nonunique_keys(self):
        """Columns are given a schema type"""
        # Integer or non-space string columns only:
        # likely keys: number of values excluding nulls is high compared to row count
        raise NotImplementedError("")

    def test_enumerations(self):
        """Columns with repeating enumerations"""
        # Integer or non-space string columns only:
        # likely enum: number of values including nulls is low compared to row count
        raise NotImplementedError("")

    def test_schema_output(self):
        """Schema can be printed out based on parsimony"""
        # Save schema to file (json schema, xsd, etc).
        raise NotImplementedError("")

    def test_validation(self):
        """Original data valid against generated schema"""
        raise NotImplementedError("")
