#
# Copyright (C) 2019 Dr. Maha Farhat
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#
"""
Test the Weld functionality.
"""

import os
import unittest

from enrich.progress import Recorder

TEST_ROOT = os.path.abspath(os.path.dirname(__file__))

class RecorderTestCase(unittest.TestCase):
    """Recording items"""
    maxDiff = 4096

    def test_return_values(self):
        """Logging returns correct responses"""
        rec = Recorder()
        self.assertFalse(rec.log('FOO', False, key='foo1'))
        self.assertTrue(rec.log('FOO', True, key='foo2'))
        self.assertTrue(rec.changed('FOO', '5', '6', key='foo4'))
        self.assertFalse(rec.changed('FOO', '5', '5', key='foo5'))

    def setUp(self):
        rec = Recorder()
        rec.explaination('FOO', "The FOO failed to BAR.")
        rec.explaination('BAR', "The BAR failed to FOO. (critical hit!)", pad=1)
        rec.log('FOO', False, key='foo1')
        rec.log('FOO', True, key='foo2')
        rec.log('FOO', True, key='foo3')
        rec.log('BAR', False, key='foo1')
        rec.log('BAR', True, key='foo2')
        rec.log('FOO', True, key='foo3')
        rec.changed('FOO', '4', '5', key='foo3')
        rec.changed('FOO', '5', '6', key='foo4')
        rec.changed('FOO', '5', '5', key='foo5')
        self.rec = rec


    def test_recorder_text(self):
        """Recorder text items"""
        log = dict(self.rec.text_items())
        self.assertEqual(log['FOO'][0], 'foo1')
        self.assertEqual(log['FOO'][1], 'foo2\nfoo3\nfoo3')
        self.assertEqual(log['FOO'][2], 'foo3:4->5\nfoo4:5->6')
        self.assertEqual(log['BAR'], ('foo1', 'foo2', ''))

    def test_recorder_status(self):
        """Status output"""
        self.assertEqual(self.rec.status(), '  ' + """
  Passed  Stage                                       Discarded    Modified
--------  ----------------------------------------  -----------  ----------
       3  The FOO failed to BAR.                              1           2
       1  ↳ The BAR failed to FOO. (critical hit!)            1           0
""".strip())

    def test_log_write(self):
        """Recorded log output"""
