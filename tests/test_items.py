#
# Copyright (C) 2019 Dr. Maha Farhat
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#
"""
Test the Weld functionality.
"""

import os
import unittest

from enrich.io import ListOfItems

TEST_ROOT = os.path.abspath(os.path.dirname(__file__))

class ItemsTestCase(unittest.TestCase):
    """Item combination functionality"""
    def test_weld_join(self):
        """Weld join two tables together"""
        loi = ListOfItems.from_name('tests/data/weld_a.csv')
        loi_b = ListOfItems.from_name('tests/data/weld_b.csv')
        loi.weld_join('Country', loi_b, 'ISO', 'Country')
        items = loi.by_unique_key('ID')
        self.assertEqual(items[1]["Country"]["Name"], "United Kingdom")
        self.assertEqual(items[2]["Country"]["Name"], "United States")
        self.assertEqual(items[1]["Country"]["ISO"], "UK")
        self.assertEqual(items[1]["Primary Language"], "English")
        self.assertEqual(items[3]["Primary Language"], "French")
        self.assertEqual(loi.recorder.text_items_dict()['JOIN'], ('CA\nBG\nNone\n', 'UK\nUSA\nFR\nDE', ''))

    def test_weld_append(self):
        """Weld append two tables together"""
        loi = ListOfItems.from_name('tests/data/weld_a.csv')
        loi_c = ListOfItems.from_name('tests/data/weld_c.csv')
        loi.weld_append('Country', loi_c, 'Country', 'Counties')
        items = loi.by_unique_key('ID')
        self.assertEqual(items[1]["Country"], "UK")
        self.assertEqual(items[1]["Counties"][0]["Name"], "Cheshire")
        self.assertEqual(items[1]["Counties"][1]["Name"], "Kent")
        self.assertEqual(items[3]["Counties"][0]["Name"], "Avignon")
        self.assertEqual(items[3]["Counties"][1]["Name"], "Bordeaux")
        self.assertEqual(loi.recorder.text_items_dict()['APPEND'], ('CA\nBG\nNone\n', 'UK\nUSA\nFR\nDE', ''))

    def test_weld_extend(self):
        """Weld extend two tables together"""
        loi = ListOfItems.from_name('tests/data/weld_c.csv')
        loi_a = ListOfItems.from_name('tests/data/weld_a.csv')
        loi.weld_extend('Country', loi_a, 'Country')
        items = loi.by_unique_key('ID')
        self.assertEqual(items[1]["Name"], "Cheshire")
        self.assertEqual(items[1]["Country"]["Rating"], 5.0)
        self.assertEqual(items[2]["Country"]["Rating"], 5.0)
        self.assertEqual(items[3]["Country"]["Rating"], 5.0)
        self.assertEqual(items[4]["Country"]["Country"], "DE")
        self.assertEqual(items[5]["Country"]["Country"], "DE")
        self.assertEqual(items[6]["Country"]["Country"], "FR")
        self.assertEqual(loi.recorder.text_items_dict()['EXTEND'], ('', 'UK\nUK\nUK\nDE\nDE\nFR\nFR\nFR\nUSA\nUSA\nUSA', ''))

    def test_weld_group(self):
        """Weld group two tables together"""
        loi_d = ListOfItems.from_name('tests/data/weld_d.csv')
        loi_c = ListOfItems.from_name('tests/data/weld_c.csv')
        loi_d.weld_group("Country", loi_c, target="Counties")
        items = loi_d.by_unique_key("ID")
        self.assertEqual(items[1]["Language"], "English")
        self.assertEqual(items[2]["Language"], "Welsh")
        self.assertEqual(items[1]["Counties"][0]["Name"], "Cheshire")
        self.assertEqual(items[1]["Counties"][1]["Name"], "Kent")
        self.assertEqual(loi_d.recorder.text_items_dict()['GROUP'], ('', 'UK\nUK\nUK\nFR\nDE\nDE\nUSA\nUSA\nUSA', ''))
