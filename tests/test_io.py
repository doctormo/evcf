#
# Copyright (C) 2019 Dr. Maha Farhat
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.
#
"""
Test the input and output API
"""

import os
import unittest

from enrich.io import ListOfItems

TEST_ROOT = os.path.abspath(os.path.dirname(__file__))

class ListTestCase(unittest.TestCase):
    """Lists of things"""
    def test_directory_json(self):
        """A directory of json files"""
        loi = ListOfItems.from_name('tests/data/dir_json')
        pack = loi.by_unique_key('id')
        self.assertEqual(len(pack), 3)
        self.assertEqual(pack[0]['name'], "Doctor")
        self.assertEqual(pack[3]['name'], "Richard")
        self.assertEqual(pack[5]['name'], "Bow")
        self.assertEqual(pack[0]['filename'], "a")

    def test_file_json(self):
        """A single json containing many frames/items"""
        loi = ListOfItems.from_name('tests/data/file_json.json')
        pack = loi.by_unique_key('id')
        self.assertEqual(len(pack), 3)
        self.assertEqual(pack[1]['name'], "James")
        self.assertEqual(pack[2]['name'], "Boris")
        self.assertEqual(pack[11]['name'], "David")

    def test_file_csv(self):
        """A single csv file containing many rows/items"""
        loi = ListOfItems.from_name('tests/data/weld_a.csv')
        pack = loi.by_unique_key('ID')
        self.assertEqual(len(pack), 8)
        self.assertEqual(pack[1]['Country'], "UK")
        self.assertEqual(pack[2]['Country'], "USA")
        self.assertEqual(pack[6]['Country'], "BG")
