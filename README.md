# VCF Importer

This repository contains code to enrich VCF files with metadata provided by CSV/TSV files.

See the documentation and [definition](./docs/Enriched_VCF_Format.md) for what an enriched VCF file is to learn more about how you can combine nucleotide, phenotype and strain meta data together in the same file.

These files are currently use pre-defined static headers, but could in the future be more flexible.

## Setup the environment

```
virtualenv pythonenv
./pythonenv/bin/pip install -r requirements.txt
```

This will provide you with a working python and requirements in your local directory.

## Activate environment

If you're not using an automatic python env activation then you will need to run:

```
source ./pythonenv/bin/activate
```

## Run the code

There are a few different tools for dealign with enriched VCF files.

### Creating eVCF from VCF files plus a data source

You can create evcf files by using the `evcf-create` program, this will consume a vcf file or a directory containing vcf files plus either a json or tsv meta data source, and will combine the two.

```
evcf create -d metadata.tsv -i input/vcf/files -o output/directory/
evcf create -d metadata.json -i input/vcf/files -o output/directory/
```

The converse function of creating evcf files is extracting meta data formats from existing evcf files.

```
evcf extract -i input/evcf/files -o output.tsv
evcf extract -i input/evcf/files -o output.json
evcf extract -i input/evcf/files -o plain/vcf/directory
```

The final function allows the data in any of the previous formats to be checked for consistancy and validity.

```
evcf validate metadata.tsv
evcf validate metadata.json
evcf validate input/evcf/files
```

The output can be given as a report or a detailed log. See the programs options using `--help` to see the full range of outputs.
