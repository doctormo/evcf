#
# Copyright (C) 2019  Luca Freschi
#                     Martin Owens
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Each item is a dictionary of values.
"""

from collections import defaultdict
from .progress import Recorder, optional_progress
from .io.outputs import SaveDirectory, SaveFile

def to_lower(key_field, key):
    """Key filter for item indexing"""
    return key.lower()

class Item(dict):
    pass

class KeyedItems(dict):
    """Dictionary of items (generated using Items.by_unique_key(...)"""
    def save(self, location, progress=None, limit=None):
        """Saves each item as a json file into a directory given by location"""
        if location is None:
            for _ in optional_progress(self, progress, limit=limit):
                pass # Debug
        else:
            with SaveDirectory(location) as loc:
                for key, value in optional_progress(self.items(), progress, limit=limit):
                    loc.save(key, value)

class Items(object):
    """
    Control and method to combine or filter multiple items from a generator or list.
    """
    def __init__(self):
        self.recorder = Recorder()
        self.ident = ''

    def save(self, location, progress=None, limit=None):
        """Save this list of items as a single long json file"""
        return SaveFile(location, self, progress=progress, limit=limit)

    def add_recorder(self, recorder, ident):
        """Add a recorder class which will log welding operatings (sucess vs. failures)"""
        self.recorder = recorder
        self.ident = ident

    def get_ident(self, action, others=None):
        other = others if isinstance(others, str) else getattr(others, 'ident', '')
        return f'{self.ident} {action} {other}'.strip()

    def by_unique_key(self, key_field, key_filter=None, fail=True, progress=None, limit=None):
        """
        Gather the whole iterator and index by the value in the key_field.
        """
        multiples = set()
        result = KeyedItems()
        for item in optional_progress(self, progress, limit=limit):
            key = item[key_field]
            if key_filter is not None:
                key = key_filter(key_field, key)
            if key in result or key in multiples:
                if fail:
                    raise KeyError(f"Key {key} found multiple times.")
                else:
                    multiples.add(key)
                    # All multiples get removed, they are bad data.
                    result.pop(key, None)
                    self.recorder.discard(self.get_ident('UNIQUE', key_field), key=key)
                    continue
            result[key] = item
        return result

    def by_array_key(self, key_field, key_filter=None):
        """
        Gather whole iterator and create a list of items with the value in key_field.
        """
        result = defaultdict(list)
        for item in self:
            key = item.get(key_field, None)
            if key is not None:
                if key_filter is not None:
                    key = key_filter(key_field, key)
                result[key].append(item)
        return result

    def weld_join(self, key_a, others, key_b=None, target=None):
        """
        Join the two rows, both must be unique.

         * key_a - Field in this list of items to match
         * others - Another list of items
         * key_b - Field in the others to match (default: key_a)
         * target - The placement in the new structure (default: [root merge])

        """
        key_b = key_b or key_a

        # XXX This will process, index and report key issues automatically
        this = self.by_unique_key(key_a, fail=False)
        others = others.by_unique_key(key_b, fail=False)

        for key in this:
            if key in others:
                if target:
                    other = others[key]
                    this[key][target] = others[key]
                else:
                    this[key].update(others[key])
                self.recorder.passed(self.get_ident('JOIN', others), key=key)
            else:
                self.recorder.conclusion(self.get_ident('JOIN', others), key=key)

    def weld_append(self, key_a, others, key_b=None, target=None):
        """
        Add colB to a list inside colA's data structure.

         * key_a - Field in this list of items to match
         * others - Another list of items
         * key_b - Field in the others to match (default: key_a)
         * target - List in the new structure (default: key_b)

        """
        key_b = key_b or key_a
        target = target or key_b

        # XXX This will process, index and report key issues automatically
        this = self.by_unique_key(key_a, fail=False)
        others = others.by_array_key(key_b)

        for key in this:
            if key in others:
                this[key][target] = others[key]
                self.recorder.passed(self.get_ident('APPEND', others), key=key)
            else:
                self.recorder.conclusion(self.get_ident('APPEND', others), key=key)

    def weld_extend(self, key_a, others, key_b=None, target=None):
        """
        Copy colB into every colA instance. Only the second must be unique.

         * key_a - Field in this list of items to replace (id to sub-item)
         * others - Another list of items
         * key_b - Field in the others to match (default: key_a)
         * target - List in the new structure (default: key_b)

        """
        key_b = key_b or key_a
        target = target or key_b

        others = others.by_unique_key(key_b, fail=False)
        for item in self:
            key = item.get(key_a, None)
            item[target] = others.get(key, None)
            if item[target] is not None:
                self.recorder.passed(self.get_ident('EXTEND', others), key=key)
            else:
                self.recorder.conclusion(self.get_ident('EXTEND', others), key=key)

    def weld_group(self, key_a, others, key_b=None, target=None):
        """
        Join every row in both A and B where the columns match (many to many)

         * key_a - Field in this list of items to replace (id to sub-item)
         * others - Another list of items
         * key_b - Field in the others to match (default: key_a)
         * target - List in the new structure (default: key_b)

        """
        key_b = key_b or key_a
        target = target or key_b

        others = others.by_array_key(key_b)
        for item in self:
            key = item.get(key_a, None)
            item[target] = others.get(key, None)
            if item[target] is not None:
                self.recorder.passed(self.get_ident('GROUP', others), key=key)
            else:
                self.recorder.conclusion(self.get_ident('GROUP', others), key=key)
