#
# Copyright (C) 2019  Martin Owens
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Basic typing takes collections and tries to turn them
from simple unicode strings, into basic types such as int, bool, float, etc.

This is not needed for json files, which are expected to be
type one already. But CSV files will contain only strings
the parsimony typer attempts to fix that.
"""

# Decide when there's ambiguity.
known_types = {
    ('bool', 'int'): int,
    ('float', 'int'): float,
    ('bool', 'float'): float,
    ('bool', 'float', 'int'): float,
}

class BasicGenerator(object):
    """
    Feed in generator, takes a list of values (not dictionary) and
    outputs the converted types.
    """
    def __init__(self, generator, litmus_count=30, is_null=True):
        self.cache = []
        self.types = []
        self.generator = generator
        self.count = litmus_count
        self.is_null = is_null

    def __iter__(self):
        if not self.types:
            self.generate_types()
        for row in self.cache:
            yield self._convert(row)
        # Yield the rest of the values
        for row in self.generator:
            yield self._convert(row)

    def _convert(self, values):
        """Convert each value into the needed type"""
        return [self.types[col](value) for col, value in enumerate(values)]

    def generate_types(self):
        """Takes the first number of rows to detect the types"""
        try:
            for row_number in range(self.count):
                self.cache.append(list(next(self.generator)))
        except StopIteration:
            pass

        for col in zip(*self.cache):
            self.types.append(self.generate_type(col))

    def generate_type(self, col):
        """Takes a list of values, and decides what type it should be"""
        could_be = set([])
        is_null = self.is_null
        for value in col:
            value = value.lower()
            if value in ('', '-', 'null'):
                is_null = True
            elif value in ('false', 'true', '1', '0'):
                could_be.add(bool)
            elif value.lstrip('-').isdecimal():
                could_be.add(int)
            elif is_float(value):
                could_be.add(float)
            else:
                could_be.add(str)

        known_key = tuple(sorted([t.__name__ for t in could_be]))
        final_type = str # default type
        if final_type not in could_be:
            if len(could_be) == 1:
                final_type = could_be.pop()
            elif known_key in known_types:
                final_type = known_types[known_key]
        if is_null:
            return type_if_not_null(final_type)
        return final_type

def is_float(val):
    """Return True if the value can be converted to a float"""
    try:
        float(val)
        return True
    except ValueError:
        return False

def type_if_not_null(target_type):
    """A method to convert to a type, except where it's a probable null value"""
    def _inner(val):
        if val.lower() in ('null', '-') or (val == '' and target_type != str):
            return None
        return target_type(val)
    return _inner
