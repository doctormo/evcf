#
# Copyright (C) 2019  Martin Owens
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Advance typing takes basically typed data and attempts to contruct a more
detailed picture of the data types.

Everything from "these are always numbers, never null" to structures within
structures of lists and dictionaries.

The resulting schema can be used as a starting point for data validation and
should be edited from there to ensure it is actually correct.
"""

class AdvancedGenerator(object):
    """
    Feed in data structures and get out schemas.
    """
    pass
