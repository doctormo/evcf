# -*- coding: utf-8 -*-
#
# Copyright 2016-2018 Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Provide logging and progress functionality
"""

import os
import sys
from collections import OrderedDict, abc

try:
    from tabulate import tabulate
except ImportError:
    tabulate = None

# Add all sorts of iterators that have a length len()
ITERS = (set, list, dict, tuple, abc.ItemsView, abc.KeysView, abc.ValuesView)

def optional_progress(lst, progress, **kwargs):
    """Adds a progress bar if indicated by 'progress'"""
    # Later we could limit the use of progress bars if the output pipe is not a tty
    if progress is not None:
        return ProgressBar(lst, progress, **kwargs)
    return lst

class ProgressBar(object):
    """Creates a progress bar for this process if OUTPUT is true"""
    cache = {}
    last_bar = None

    def __init__(self, lst, label, done=None, limit=None, to=sys.stdout):
        self.lst = lst
        self._io = to
        self.done = done
        self.label = label
        self.limit = limit

        self.length = None
        if hasattr(lst, 'length'):
            self.length = lst.length
        elif isinstance(lst, ITERS):
            self.length = len(lst)
        elif hasattr(lst, 'fileno'):
            self.length = os.fstat(lst.fileno()).st_size / 1024
        elif hasattr(lst, 'filename'):
            self.length = os.stat(lst.filename).st_size / 1024
        self._work = self.generator()

    def __iter__(self):
        return self
  
    def __next__(self):
        return next(self._work)

    def __getattr__(self, attr):
        if attr in self.__dict__:
            return getattr(self, attr)
        return getattr(self.lst, attr)

    def generator(self):
        tell = 0
        for count, item in enumerate(self.lst):
            if isinstance(item, bytes):
                tell += len(item)
            elif isinstance(item, str):
                tell += len(item.encode('utf-8'))
            # This count is pre-filtering, so it gives out progress bar more accuracy
            if hasattr(self.lst, 'pos') and isinstance(self.lst.pos, int):
                count = self.lst.pos
            self.update(count, tell=tell)
            yield item
            if self.limit is not None and count >= self.limit:
                break
        self.finish()

    def update(self, pos, tell=None):
        """Update the progressbar"""
        if self.last_bar and self.last_bar != self.label:
            self._io.write("\n")
        self.last_bar = self.label

        if tell and self.limit is None:
            tell = self.length * 1024 if tell is None else float(tell)
            self.done = pos
            self._update(tell / self.length / 1024, int(tell / 1024), unit='KB')
        elif isinstance(pos, int):
            length = self.limit or self.length
            if length:
                self._update(float(pos) / length, pos)
            else:
                self._update(float((pos / 10) % 200) / 200, pos, done='-')

        elif isinstance(pos, float):
            if self.length:
                self._update(pos, int(self.length * pos))
            else:
                self._update(pos, pos)

    def get_done(self):
        """Return the number of completed lines"""
        if isinstance(self.done, ITERS):
            return len(self.done)
        elif isinstance(self.done, int):
            return self.done
        elif callable(self.done):
            return self.done()
        return '-'

    def _update(self, loc, count, done='=', todo='-', unit=''): #pylint: disable=too-many-arguments
        label = "{:>8} ".format(self.label)

        if self.cache.get(label, -1) != int(loc * 200):
            self._io.write(label)
            self._io.write('|' + (done * int(loc * 50)))
            self._io.write((todo * (50 - int(loc * 50))) + '| ')
            self._io.write('{:d}% ({}{}/{})\r'.format(int(loc * 100), count, unit, self.get_done()))
            self._io.flush()
            self.cache[label] = int(loc * 200)

    def finish(self):
        """Finish the writing"""
        self.update(1.0)
        self.last_bar = None
        self._io.write("\n")


class Recorder(OrderedDict):
    """Record the completion or successful vetting of records at various stages"""
    def __init__(self):
        super(Recorder, self).__init__()
        self.messages = dict()

    def __missing__(self, key):
        self[key] = value = (list(), list(), list())
        return value

    def explaination(self, kind='OK', msg=None, pad=0):
        """Adds an explaination message and sets the order"""
        if pad and msg:
            msg = ('  ' * (pad - 1)) + u'↳ ' + msg
        self.messages[kind] = msg

    def conclusion(self, kind, **kw):
        """The given record is concluded and no more processing will be done"""
        self.log(kind, passed=False, **kw)

    def discard(self, kind, **kw):
        """Like conclusion but sets passed to '-' (NULL)"""
        if self[kind][1] is not None:
            self[kind] = (self[kind][0], None)
        self.conclusion(kind, **kw)

    def passed(self, kind, **kw):
        """The given record is passed a test and is going to continue"""
        self.log(kind, passed=True, **kw)

    def log(self, kind, passed=True, **kw):
        """Log the record as either passed or concluded"""
        if kind is not None:
            self[kind][bool(passed)].append(self.get_log(**kw))
        return passed

    def changed(self, kind, val_from, val_to, **kw):
        """Log that a record was changed in a consistant way"""
        if kind is not None:
            if val_from != val_to:
                self[kind][2].append(self.get_log(**kw) + f':{val_from}->{val_to}')
                return True
            self[kind][True].append(self.get_log(**kw))
        return False

    @staticmethod
    def get_log(**kw):
        """Generates a log message given these items"""
        if len(kw) == 1:
            for value in kw.values():
                return str(value)
        elif len(kw) > 1:
            return str(kw)[1:-1]
        raise KeyError("Nothing to log, how would I track this?")

    def write(self, template):
        """Write out every one of the logs to the template's file location"""
        for name, lists in self.text_items():
            for log, lst in zip(['failed', 'passed', 'changed'], lists):
                if not lst:
                    continue
                with open(self._log(template, name, log), 'w') as fhl:
                    fhl.write(lst)

    @staticmethod
    def _log(template, name, log):
        """Generate a log filename and create an directires if needed"""
        filename = template.format(name.lower(), log)
        dirname = os.path.dirname(filename)
        if not os.path.isdir(dirname):
            os.makedirs(dirname)
        return filename

    def text_items(self):
        """Output items compiled into text"""
        for name, (lst_a, lst_b, lst_c) in self.items():
            txt_a = '' if lst_a is None else '\n'.join(lst_a)
            txt_b = '' if lst_b is None else '\n'.join(lst_b)
            txt_c = '' if lst_c is None else '\n'.join(lst_c)
            yield (name, (txt_a, txt_b, txt_c))

    text_items_dict = lambda self: OrderedDict(self.text_items())

    def status(self):
        """Print out a status counts table"""
        if tabulate is None:
            raise ImportError("Tabulate is required for output status.")
        rows = []
        for key, (lst_a, lst_b, lst_c) in self.items():
            if lst_a or lst_b or lst_c:
                len_a = '-' if lst_a is None else len(lst_a)
                len_b = '-' if lst_b is None else len(lst_b)
                len_c = '-' if lst_c is None else len(lst_c)
                rows.append([len_b, self.messages.get(key, key), len_a, len_c])
        return tabulate(rows, headers=['Passed', 'Stage', 'Discarded', 'Modified'])
