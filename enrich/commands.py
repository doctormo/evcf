#!/usr/bin/env python
#
# Copyright (C) 2019 - Martin Owens
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Base command line tools which are used to contract your command line programs.
"""
import os
import sys
import inspect

from argparse import ArgumentParser, ArgumentTypeError

def get_parser(desc, *commands):
    """
    Generate a whole argument parser for each of the sub commands.
    """
    ret = {}
    parser = ArgumentParser(description=desc)
    # parser.add_argument('-g', '--global')
    subparsers = parser.add_subparsers(
        dest="command",
        title="Commands",
        description=None,
        help='')
    for cmd in commands:
        if hasattr(cmd, 'command') and cmd.command:
            sub_parser = subparsers.add_parser(cmd.command, **cmd.cmdkw)
            cmd.add_args(sub_parser)
            ret[cmd.command] = cmd
            for alias in cmd.cmdkw.get('aliases', []):
                ret[alias] = cmd
    return parser, ret

def multi_command_runner(desc, *commands, fallback=None):
    """
    Run a group of commands, each containing a single subcommand based on BaseCommand.
    """
    parser, commands = get_parser(desc, *commands)
    args = vars(parser.parse_args())
    command = args.pop('command')
    if command is None:
        parser.print_help()
    if command not in commands:
        if fallback:
            fallback(**args).arg_run()
        else:
            parser.print_help()
    else:
        commands[command](**args).arg_run()


class BaseCommand(object):
    """
    Each command in the evcf tool should inherit from this base class
    and provide at least a run() function but also add_args() to
    define further inputs.
    """
    command = ''
    cmdkw = {}

    def __init__(self, **kwargs):
        self.options = kwargs

    def arg_run(self):
        """Run based on the argument specification, calls run()"""
        try:
            spec = inspect.getfullargspec(self.run)
            kwargs = {}
            if spec.varkw:
                kwargs = self.options
            elif spec.args:
                kwargs = dict([(name, self.options[name])\
                    for name in self.options if name in spec.args])
            self.run(**kwargs)
        except IOError as err:
            sys.stderr.write("Fatal IO Error: {}\n".format(err))

    def run(self):
        """
        A command run command is the main entry point into that process.

        The arguments can optionally be the named options given to the argument
        parser, and they will be populated as needed.
        """
        raise NotImplementedError("You must have a run")

    @classmethod
    def add_args(cls, parser):
        """Used to add arguments to the command parser, over-ride when needed."""
        return parser

    @staticmethod
    def arg_filename(name):
        """A real and existing filename on the disk, returns a full path"""
        fullname = os.path.expanduser(name)
        fullname = os.path.abspath(fullname)
        if not os.path.isfile(fullname):
            raise ArgumentTypeError("File not found: %s" % name)
        return fullname
