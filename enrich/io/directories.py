#
# Copyright (C) 2019  Luca Freschi
#                     Martin Owens
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Directories full of files
"""
import os
import sys

from ..items import Item
from .inputs import ListOfItems
from .utils import path

class BaseDirectory(ListOfItems):
    file_ext = '^'

    def __init__(self, path):
        super().__init__()
        self.path = path

    @classmethod
    def can_handle(cls, name):
        dname = path(name)
        if os.path.isdir(dname):
            for fname in os.listdir(dname):
                fpath = os.path.join(dname, fname)
                if cls.can_handle_file(fpath):
                    return cls(dname)
        return False

    @classmethod
    def can_handle_file(cls, fname):
        return any([fname.endswith(ext) for ext in cls.file_ext])

    def iterator(self):
        """Loops through each file in the directory and returns"""
        for fname in os.listdir(self.path):
            fpath = os.path.join(self.path, fname)
            if self.can_handle_file(fpath):
                try:
                    ret = self.get_file(fpath)
                    ret['filename'] = fname.rsplit('.', 1)[0]
                    yield ret
                except IOError:
                    pass

    def get_file(self, fname):
        raise NotImplementedError("Child class must provide a way of opening the file.")


class JsonDirectory(BaseDirectory):
    """A directory full of json files"""
    file_ext = ('.json', '.js')

    def get_file(self, json_filename):
        """Open the json file and check it's a dictionary"""
        import json
        with open(json_filename, 'r') as fhl:
            try:
                return Item(json.loads(fhl.read()))
            except json.decoder.JSONDecodeError as err:
                return Item({'error': str(err)})

class YamlDirectory(BaseDirectory):
    """A directory full of yaml files"""
    file_ext = '.yaml',

    def get_file(self, yaml_filename):
        import yaml
        with open(yaml_filename, 'r') as fhl:
            return Item(yaml.load(fhl.read()))
