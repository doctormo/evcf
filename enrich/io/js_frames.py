#
# Copyright (C) 2019  Luca Freschi
#                     Martin Owens
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
A Json File
"""
import os
import sys
import json

from ..items import Item
from .inputs import ListOfItems
from .utils import path

class JsonFile(ListOfItems):
    @classmethod
    def can_handle(cls, name):
        fname = path(name)
        if os.path.isfile(fname) and fname.endswith('.js') or fname.endswith('.json'):
            return cls(fname)
        return False

    def __init__(self, filename):
        super().__init__()
        # TODO: upgrade this with json-streamer to make it more effeciant with large files.
        self.filename = filename
        with open(filename, 'rb') as fhl:
            self.data = json.loads(fhl.read())
        if isinstance(self.data, dict):
            raise IOError("Json file should contain a list of objects, not be an object itself.")

    def iterator(self):
        """Loops through each file in the directory and returns"""
        for item in self.data:
            yield Item(item)
