#
# Copyright (C) 2019  Luca Freschi
#                     Martin Owens
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
A CSV File
"""
import os
import sys
from io import StringIO
from collections import defaultdict
from csv import reader as CsvReader

from ..parsimony.basic import BasicGenerator
from ..progress import optional_progress
from ..items import Item
from .inputs import ListOfItems
from .utils import path

class CsvFile(ListOfItems):
    @classmethod
    def can_handle(cls, name):
        fname = path(name)
        if os.path.isfile(fname):
            if fname.endswith('.csv'):
                return cls(fname, delimiter=',')
            elif fname.endswith('.tsv'):
                return cls(fname, delimiter='\t')
            elif fname.endswith('.txt'):
                return cls(fname, delimiter=None)
        return False

    def detect_delimiter(self, sniff=5, threshold=3):
        """Attempt to detect the delimiter being used in an anon text file"""
        candidates = ',;:|\t'
        with open(self.filename, 'r') as fhl:
            counts = defaultdict(list)
            for x, line in enumerate(fhl):
                if x >= sniff:
                    break
                for char in candidates:
                    counts[char].append(line.count(char))
            # Pure search (every line has the same number of delimiters) 
            for char in candidates:
                counter = tuple(set(counts[char]))
                if len(counter) == 1 and counter[0] >= threshold:
                    return char
        raise ValueError(f"Can't detect the delimiter for file: {self.filename}")

    def __init__(self, filename, delimiter=','):
        super().__init__()
        self.filename = filename
        if delimiter is None:
            delimiter = self.detect_delimiter()

        self.delimiter = delimiter
        self.handle = None
        self.reader = None
        self._header = []

    def open_file(self):
        """Open the file for reading"""
        self.close_file()
        self.handle = optional_progress(open(self.filename, 'r'), self.progress, limit=self.limit)
        self.reader = CsvReader(self.handle, delimiter=self.delimiter)
        self._header = next(self.reader)

    def add_progress(self, name, limit=None):
        """Add progress bar to opening the sv file"""
        super().add_progress(name, limit=limit)
        # Re-open the file if adding a progress bar after already opening it.
        if self.handle:
            self.open_file()

    def close_file(self):
        """Close any open file handle (used for reading)"""
        if self.handle is not None:
            try:
                self.handle.close()
                self.handle = None
            except (IOError, OSError):
                pass

    def iterator(self):
        """Loops through each file in the directory and returns"""
        self.open_file()
        try:
            for row in BasicGenerator(self.reader):
                yield Item(self.decompose(row))
        finally:
            self.close_file()

    def decompose(self, row):
        """Turn a row into a dictionary using the header"""
        # There's probably some better ways of doing this.
        return dict(zip(self._header, row))

    #def by_unique_key(self, key_field, fail=True):
    #    """Generate unique key, but save the index for later use"""
    #    basename = os.path.basename(self.filename)
    #    filename = f'.{basename}.{keyfield}.idx'
    #    key_index_filename = os.path.join(os.path.dirname(self.filename), filename)
    #    if os.path.isfile(key_index_filename): # XXX and index filename is newer than self.filename
    #        # Load existing index
    #        return IndexedItems(key_index_filename, self.filename, super().by_unique_key(key_field, fail=True))

#class IndexedItems(KeyItems):
#    """Special version of KeyItems with cached indexing"""
#    pass

class CsvString(CsvFile):
    """Take in a string as a csv data"""
    def open_file(self):
        self.reader = CsvReader(StringIO(self.filename), delimiter=self.delimiter)
        self._header = next(self.reader)
