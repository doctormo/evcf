#
# Copyright (C) 2019  Luca Freschi
#                     Martin Owens
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Consume data coming in.
"""
import sys
import atexit
from ..items import Items
from ..mutate import get_mutator
from ..validator import MetaDataValidator, ValidationStats

if sys.version_info.major < 3 or sys.version_info.minor < 6:
    ver = '.'.join([str(l) for l in sys.version_info[:3]])
    raise ImportError("This module requires at least python 3.6 (currently: {})".format(ver))

# We never get empty tuples in data, So empty tuple means "missing field"
MISSING = ()
IS_MISSING = lambda value: value == MISSING
IS_NULL = lambda value: value in (None, MISSING)
IS_EMPTY = lambda value: not value

class ListOfItems(Items):
    """
    A List of Items provides a generator interface over any kind of data
    for which a child class claims to be able to handle the input.
    """
    providers = []

    name = classmethod(lambda cls: cls.__name__)
    is_base = classmethod(lambda cls: 'Base' in cls.name())

    @classmethod
    def __init_subclass__(cls):
        # this only works in python >=3.6
        if not cls.is_base():
            ListOfItems.providers.append(cls)

    @classmethod
    def from_name(cls, name):
        tried = []
        for provider in cls.providers:
            ret = provider.can_handle(name)
            if ret not in (None, False):
                return ret
            tried.append(provider.name())
        tried = ', '.join(tried)
        raise IOError(f"Don't know how to read '{name}'; tried: {tried}")

    def __init__(self):
        self.actions = []
        self.progress = None
        self.length = None
        self.pos = None
        self.limit = None
        self._gen = None
        self._id_cols = None
        self._data_cols = []
        super().__init__()

    def __iter__(self):
        # Each iter should reset the generator
        self._gen = None
        return self

    def __next__(self):
        if self._gen is None:
            self._gen = self.generator()
        return next(self._gen)

    def generator(self):
        """Generate and cache each item yielded by upstream iterator() method"""
        # We want the items to progressively load, but we also want to cache
        # the results as they come in. Later we will have to delete items
        # which have been saved or processed.
        if not hasattr(self, '_items'):
            _iter = self._first_gen()
        else:
            _iter = self._items

        self.pos = 0
        for item in _iter:
            self.pos += 1
            for action in self.actions:
                prev = type(item).__name__
                item = action(item)
                #print("{} -> {} -> {}".format(prev, action.__name__, type(item).__name__))
                if item is None:
                    break
            if item is not None:
                yield item

    def _first_gen(self):
        self._items = []
        self.length = 0
        for item in self.iterator():
            self.length += 1
            self._items.append(item)
            yield item

    def add_field_filter(self, key_field, field_filter=IS_EMPTY, report_id=None):
        """
        Remove any rows that do not have the required field, or if the field is None etc.

        IS_EMPTY - Reject anything that is missing, None or empty (list)
        IS_NONE - Reject anything that is missing or None
        IS_MISSING - Reject anything where the field is missing only.
        """
        def _filter(row):
            return field_filter(row.get(key_field, MISSING))
        self.add_filter(_filter, report_id)

    def add_validator_filter(self, xsd_file, report_id=None):
        """
        Add a row based xsd filter, if report_id is set the error will be
        given in the report using report_id as the prefix.
        """
        validator = MetaDataValidator(xsd_file)
        validator.stats = ValidationStats()
        def print_stats():
            print(validator.stats.stats())
        atexit.register(print_stats)
        def _filter(row):
            result = validator.validate(row)
            validator.stats.append(result)
            return bool(result)
        self.add_filter(_filter, report_id)

    def add_filter(self, filter_func, report_id=None):
        """Add a filter function for this generator"""
        if not callable(filter_func):
            raise ValueError("Filter function must be callable!")
        def _filter_action(item):
            if self.recorder.log(report_id, not filter_func(item), key=self.get_item_key(item)):
                return item
        self.actions.append(_filter_action)

    def add_mutator(self, mut_file, report_id=None):
        """
        Add a mutator to the generator, each item is mutated using the script file provided.
        """
        mut_func = get_mutator(mut_file)
        def _mutation_action(item):
            return self.recorder.log(report_id, mut_func(item=item), key=self.get_item_key(item))
        self.actions.append(_mutation_action)

    def add_field_mutator(self, key_field, field_mutator, report_id=None):
        """
        Add a mutator for a single field, the mutator can be a function or a dictionary.
        """
        if isinstance(field_mutator, dict):
            def _dict_mutator(key_field, val):
                return field_mutator.get(val, val)
            return self.add_field_mutator(key_field, _dict_mutator, report_id=None)

        def _func_mutator(item):
            val = item.get(key_field, None)
            try:
                item[key_field] = field_mutator(key_field, val)
                self.recorder.changed(report_id, val, item[key_field],
                                      key=self.get_item_key(item, key_field))
                return item
            except ValueError:
                self.recorder.log(report_id, False, key=self.get_item_key(item, key_field))
                return None
        self.actions.append(_func_mutator)

    def get_item_id(self, item, *data_cols):
        """Return a unique id for reporting"""
        if not self._id_cols:
            return '-'
        for key in self._id_cols:
            if key in item:
                return item[key]
        raise KeyError("None of the id keys exist ({}) in {}".format(
            ', '.join(self._id_cols), ', '.join(list(item))))

    def get_item_key(self, item, *data_cols):
        """Return the id plus any data cols useful for logging"""
        iid = self.get_item_id(item)
        data_cols = list(data_cols) + self._data_cols
        cols = ', '.join(["{}:{}".format(col, item[col]) for col in data_cols])
        if cols:
            return f"{iid} ({cols})"
        return iid

    def set_id_cols(self, *id_cols):
        """Set the column name used in logging, each col will be tried until one exists."""
        self._id_cols = list(id_cols)

    def set_data_cols(self, *data_cols):
        """Set the column name used in logging"""
        self._data_cols = list(data_cols)

    def iterator(self):
        """
        Iterate over each item in the input, each yielded value should be
        a single dictionary item sub-classes the Item dictionary class.
        """
        name = cls.name()
        raise NotImplementedError(f"{name} needs to provide an iterator function.")

    @classmethod
    def can_handle(cls, name):
        """
        Returns the provider object if it can handle the data.

        Returns None or False if it can't.
        """
        name = cls.name()
        raise NotImplementedError(f"{name} needs to provide a can_handle function.")

    def add_progress(self, name, limit=None):
        """
        Add a progress bar to opening this file/io during looping.
        """
        # NotImplementedHere (default is not supported)
        self.progress = name
        self.limit = limit
