#
# Copyright (C) 2019  Luca Freschi
#                     Martin Owens
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Output all the item data (to json)
"""
import os
import sys
import json

from ..progress import optional_progress

class SaveDirectory(object):
    """
    Save each item into it's own file in the given directory.
    """
    def __init__(self, location):
        self.dirname = location

    def __enter__(self):
        if not os.path.isdir(self.dirname):
            os.makedirs(self.dirname)
        return self

    def __exit__(self, exc, value, traceback):
        self.dirname = None

    def save(self, key, value):
        """Save a single file into the directory"""
        if self.dirname is not None and os.path.isdir(self.dirname):
            filename = os.path.join(self.dirname, key + '.json')
            with open(filename, 'w') as fhl:
                fhl.write(json.dumps(value, indent=2))

class SaveFile(object):
    """
    Save all items into a single file using a progressive saver.
    """
    def __init__(self, location, items=None, progress=None, limit=None):
        self.progress = progress
        self.limit = limit
        if os.path.exists(location):
            raise IOError(f"Will not over-write filename: {location}")
        dirname = os.path.dirname(location)
        if not os.path.isdir(dirname):
            raise IOError(f"Can not save to non-existant parent directory: {dirname}")
        self.filename = location
        if items is not None:
            self.save(items)
    
    def save(self, items):
        # Progressively save each item
        with open(self.filename, 'w') as fhl:
            fhl.write('[\n')
            for x, item in enumerate(optional_progress(items, self.progress, limit=self.limit)):
                if x > 0:
                    fhl.write(',\n')
                fhl.write(json.dumps(item, indent=2))
            fhl.write('\n]')
