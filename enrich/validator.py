#
# Copyright (C) 2018  Dr. Maha Farhat
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Validate the metadata structure stored or added to vcf files.
"""
import os
from collections import defaultdict

from xssd import Validator
from xssd.errors import NO_ERROR, EMPTY_OK, INVALID_ENUMERATION, ValidateError, ElementErrors

#from .loc import CountryLookup

INVALID_COUNTRY = ValidateError.new(0x02, 'Invalid Country', 'Country not found in ISO Database')
INVALID_LOCODE = ValidateError.new(0x02, 'Invalid LOCODE', 'Invalid LOCODE not found in Database')
INVALID_STATE = ValidateError.new(0x02, 'Invalid State', 'State not found in LOCODE Database')
INVALID_CITY = ValidateError.new(0x02, 'Invalid City', 'City not found in LOCODE Database')

class MetaDataValidator(Validator):
    """Create a validator instance to validate your meta data structure against the xsd schema"""
    def __init__(self, schema, strict_exist=True, debug=True):
        self._countries = None

        super(MetaDataValidator, self).__init__(
            definition=schema,
            strict_root=False,
            strict_exist=strict_exist,
            debug=debug,
        )
        self.update_types({
            'simpleTypes':{
                'LOCODE_COUNTRY': {'custom' : self.valid_locode_country},
                'LOCODE_STATE': {'custom' : self.valid_locode_state},
                'LOCODE_CITY': {'custom' : self.valid_locode_city},
                'LOCODE': {'custom' : self.valid_locode},
            }
        })

    def validate(self, data, root='root'):
        """
        Allow the strain root element to be optional
        """
        data = self._load_file(data)
        if root not in data:
            ret = self.validate({'root': {root: data}})
            return ret.get(root, {})
        return super(MetaDataValidator, self).validate(data)

    def valid_locode_country(self, country, simple_type=None):
        """
        Validate a country name against the countries database.
        """
        return NO_ERROR()
        #if self._countries is None:
        #    self._countries = CountryLookup()
        if country in self._countries.name: # pylint: disable=no-member
            return NO_ERROR()
        if country in self._countries: # alpha_2
            return NO_ERROR()
        if country in self._countries.alpha_3: # pylint: disable=no-member
            return NO_ERROR()
        return INVALID_COUNTRY()

    def valid_locode_state(self, state, simple_type=None):
        """
        Validate the state name against the states database.
        """
        #raise NotImplementedError("Need to write this")
        return NO_ERROR()

    def valid_locode_city(self, city, simple_type=None):
        """
        Validate the city name against the city database.
        """
        #raise NotImplementedError("Need to write this")
        return NO_ERROR()

    def valid_locode(self, locode, simple_type=None):
        """
        Validate the LOCODE against the LOCODE database.
        """
        #raise NotImplementedError("Need to write this")
        return NO_ERROR()

class ValidationStats(list):
    """
    A list of validation errors which can be printed as statistics
    about a set of data taken together.
    """
    def stats(self, ok=False):
        """
        Caluclate the list of validation error structures into
        a printable output. Returns a string.
        """
        ret = ""
        matrix = self.count_matrix(prefix='all', ok=ok)
        for name in sorted(matrix):
            errs = "; ".join(["{}: {}".format(err, matrix[name][err]) for err in sorted(matrix[name])])
            tab = "  " * name.count('__')
            ret += "{}{} ({})\n".format(tab, name.split('__')[-1], errs)
        return ret

    def count_matrix(self, prefix='root', ok=False):
        """
        Gets the counts, but then formats them into a matrix of errors per field.
        """
        matrix = defaultdict(lambda: defaultdict(int))
        counts = self.counts(prefix=prefix, ok=ok)
        for name in sorted(counts):
            count = counts[name]
            (name, error) = name.split(':')
            matrix[name][error] = count
        return matrix

    def counts(self, prefix='root', ok=False):
        """
        Calculate the list of validation error structures
        into a set of counts and messages
        """
        store = defaultdict(int)
        for validation in self:
            self._count(validation, store, prefix, ok=ok)
        return store

    def _count(self, errors, store, prefix, ok=False):
        if isinstance(errors, list):
            for suberrors in errors:
                self._count(suberrors, store, prefix, ok=ok)

        elif isinstance(errors, ElementErrors):
            for name, children in errors.items():
                fullname = "{}__{}".format(prefix, name).strip('_')
                self._count(children, store, prefix=fullname)

        elif ok or not isinstance(errors, (NO_ERROR, EMPTY_OK)):
            if hasattr(errors, 'label'):
                store_id = '{}:{}'.format(prefix, errors.label()).strip(':')
                if isinstance(errors, INVALID_ENUMERATION):
                    store_id += ' == ' + str(errors.context)
                store[store_id] += 1
